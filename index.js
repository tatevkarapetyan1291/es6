function isValidJSON(json) {
    try {
        JSON.parse(json);
        return true;
    } catch (e) {
        return false;
    }
}
isValidJSON('{"a": 2}'); // result: true;
isValidJSON('{"a: 2'); // result: false;


//Greeting

function greeting({name,surname,age}) {
 return `Hello, my name is ${name} ${surname}  and I am ${age} years old`
}
greeting({name: 'Bill', surname: 'Jacobson', age: 33}); // result: Hello, my name is Bill Jacobson and I am 33 years old.
greeting({name: 'Jim', surname: 'Power', age: 11}); // result: Hello, my name is Jim Power and I am 11 years old.

//Unique nambers

function unique(array) {
    return [...new Set(array)];
}
console.log(unique([1, 1, 2, 3, 5, 4, 5]));

//Generator

function* generator(arr) {
    let index = 0;
  while(index < arr.length){
      yield arr[index];
      index++;
    }
  }

const it = generator(['brick', 'plate', 'minifigure', 'slope']);
it.next().value; // return 'brick'
it.next().value; // return 'plate'